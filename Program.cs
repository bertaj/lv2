using System;

namespace LV2
{
    class Program
    {
        static void Main(string[] args)
        {
            DiceRoller roller = new DiceRoller();
            for(int i=0; i < 20; i++)
            {
                roller.InsertDie(new Die(6));
            }
            roller.RollAllDice();
            foreach(int rolled in roller.GetRollingResults())
            {
                Console.WriteLine(rolled.ToString());
            }
        }
    }
}
